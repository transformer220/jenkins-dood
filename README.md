# Jenkins-DooD


This makes you able to run Jenkins in Docker and to run new Docker containers for Jenkins jobs in your docker host. for example...

* pull docker images
* create and run docker containers
* build docker images
* push your docker image

This dockerfile is based on [jenkins/jenkins](https://hub.docker.com/r/jenkins/jenkins/) and install Docker CE with [Docker install Guide](https://docs.docker.com/engine/installation/linux/docker-ce/debian/).


**Tested on Ubuntu 20.04.1 LTS and support docker images **
```
jenkins/jenkins:latest
jenkins/jenkins:slim
```
## Getting Started
### 1. Pull Code

From [Github Repository](https://gitlab.com/transformer220/jenkins-dood.git)

```
$ git clone https://gitlab.com/transformer220/jenkins-dood.git
$ cd jenkins-dood
```

### 2. Create docker container and Start Jenkins

#### For Linux User

```
$ docker-compose up -d
```

After a while, you can access http://localhost:8080

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [Using Docker-in-Docker for your CI or testing environment? Think twice. ](https://jpetazzo.github.io/2015/09/03/do-not-use-docker-in-docker-for-ci/)